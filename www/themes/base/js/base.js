/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {

    /**
     * The homepage carousel
     */
    Drupal.behaviors.homeCarousel = {
        attach: function (context, settings) {
            var swiper = new Swiper('.home-carousel', {
                effect: 'fade',
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true
                },
                autoplay: {
                    delay: 2500,
                    disableOnInteraction: false
                }
            });

            $(".home-carousel").hover(function() {
                swiper.autoplay.stop();
            }, function() {
                swiper.autoplay.start();
            });
        }
    };

    /**
     * Events on the right off canvas container.
     */
    Drupal.behaviors.rightOffCanvasBehaviour = {
        attach: function (context, settings) {
            // Apply is-active class to menu toggle button.
            $('#right-off-canvas-menu')
                .on('opened.zf.offcanvas', function (e) {
                    $('button[data-toggle="right-off-canvas-menu"]').addClass('is-active');
                })
                .on('closed.zf.offcanvas', function (e) {
                    $('button[data-toggle="right-off-canvas-menu"]').removeClass('is-active');
                })
            ;

        }
    };

    /*
     * Provides increment and decrement icons on product quantity field.
     */
    Drupal.behaviors.productQuantityIncrement = {
        attach: function (context, settings) {

            $(".view-commerce-cart-form form .field--name-quantity .js-form-type-number").once('productQuantityIncrement').append('<div class="suffix">pers</div><div class="inc-buttons"><div class="inc qty-button">+</div><div class="dec qty-button">-</div></div>');

            $(".product-default .views-field-edit-quantity .js-form-type-number").once('productQuantityIncrement').append('<div class="suffix">pers</div><div class="inc-buttons"><div class="inc qty-button">+</div><div class="dec qty-button">-</div></div>');
            $(".product-normal .views-field-edit-quantity .js-form-type-number").once('productQuantityIncrement').append('<div class="inc-buttons"><div class="inc qty-button">+</div><div class="dec qty-button">-</div></div>');

            $(".product-normal .commerce-order-item-add-to-cart-form .field--name-quantity .form-item input").once('productQuantityIncrement').after('<div class="inc-buttons"><div class="inc qty-button">+</div><div class="dec qty-button">-</div></div>');
            $(".product-default .commerce-order-item-add-to-cart-form .field--name-quantity .form-item input").once('productQuantityIncrement').after('<div class="suffix">pers</div><div class="inc-buttons"><div class="inc qty-button">+</div><div class="dec qty-button">-</div></div>');

            $(".qty-button").once().click(function () {
                var $button = $(this);
                var oldValue = $button.parent().parent().find("input").val();

                if ($button.text() == "+") {
                    var newVal = parseFloat(oldValue) + 1;
                } else {
                    // Don't allow decrementing below zero
                    if (oldValue > 0) {
                        var newVal = parseFloat(oldValue) - 1;
                    } else {
                        newVal = 0;
                    }
                }
                $button.parent().parent().find("input").val(newVal);

            });
        }
    };

    /*
     * Behavior for the productfilters on mobile devices.
     */
    Drupal.behaviors.productsFilterControls = {
        attach: function (context, settings) {
            $('.close-filters').click(function(){
                if($(window).width() >= 900){
                    $('.filter-region-left').addClass('closed');
                    $('.main-region').addClass('filters-closed');

                    setTimeout(function(){
                    }, 200);
                } else {
                    $('.filter-region-left').removeClass('open');
                    $('.main-region').addClass('filters-closed');
                    $('.main-region').removeClass('filters-open');
                }
            });

            $('.open-filters').click(function(){
                if($(window).width() >= 900) {
                    $('.main-region').removeClass('filters-closed');
                    $('.filter-region-left').removeClass('closed');
                } else {
                    $('.main-region').removeClass('filters-closed');
                    $('.main-region').addClass('filters-open');
                    $('.filter-region-left').addClass('open');
                }
            });
        }
    };

  Drupal.behaviors.privacyCheckbox = {
    attach: function (context, settings) {
        if($('#edit-sidebar-privacy').length){
            $('#edit-actions-next').click(function(){
                console.log('click');
              if($('#edit-sidebar-privacy:checked').length <= 0){
                // show message
                if($('.privacy-notification').length){
                  $('.privacy-notification').text('Gelieve eerst akkoord te gaan met de privacy policy.');
                } else {
                  $('#edit-actions-next').parent().append('<span class="privacy-notification">Gelieve eerst akkoord te gaan met de privacy policy.</span>');
                }
              }
            });
        }
    }
  }

})(jQuery, Drupal);
