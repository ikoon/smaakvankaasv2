<?php

namespace Drupal\mvi_commerce\Plugin\views\field;

use Drupal\commerce_order\Entity\OrderItem;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form element displaying the order item quantity.
 *
 * @ViewsField("custom_order_item_quantity")
 */
class CustomQuantity extends FieldPluginBase {

    /**
     * Constructs a new EditQuantity object.
     *
     * @param array $configuration
     *   A configuration array containing information about the plugin instance.
     * @param string $plugin_id
     *   The plugin ID for the plugin instance.
     * @param mixed $plugin_definition
     *   The plugin implementation definition.
     * @param \Drupal\commerce_cart\CartManagerInterface $cart_manager
     *   The cart manager.
     */
    public function __construct(array $configuration, $plugin_id, $plugin_definition) {
        parent::__construct($configuration, $plugin_id, $plugin_definition);
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition
        );
    }

    /**
     * {@inheritdoc}
     */
    public function render(ResultRow $values) {

        $value = $this->getValue($values);
        $orderItem = null;

        /* @var OrderItem $orderItem */
        if(array_key_exists('order_items', $values->_relationship_entities)){
            $orderItem = $values->_relationship_entities['order_items'];
        } else if ($values->_entity->getEntityTypeId() == 'commerce_order_item'){
            $orderItem = $values->_entity;
        }

        if($orderItem){
            $purchasedEntity = $orderItem->getPurchasedEntity();
            $productType = $purchasedEntity->bundle();

            if($productType == 'default'){
              if((int)$value == 1){
                $value = (int)$value . ' persoon';
              } else {
                $value = (int)$value . ' personen';
              }
            } else {
              if((int)$value == 1){
                $value = (int)$value . ' stuk';
              } else {
                $value = (int)$value . ' stuks';
              }
            }
        } else {
            $orderItem = $values->_entity;
            $purchasedEntity = $orderItem->getPurchasedEntity();
            $productType = $purchasedEntity->bundle();

            if($productType == 'default'){
              if((int)$value == 1){
                $value = (int)$value . ' persoon';
              } else {
                $value = (int)$value . ' personen';
              }
            } else {
              if((int)$value == 1){
                $value = (int)$value . ' stuk';
              } else {
                $value = (int)$value . ' stuks';
              }
            }
        }

        return $this->sanitizeValue($value);
    }

}