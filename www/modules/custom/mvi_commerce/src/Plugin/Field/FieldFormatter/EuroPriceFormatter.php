<?php

namespace Drupal\mvi_commerce\Plugin\Field\FieldFormatter;

use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use Drupal\commerce\Context;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_price\Resolver\ChainPriceResolverInterface;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\commerce_price\Plugin\Field\FieldFormatter\PriceCalculatedFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'euro_price_formatter' to show the €sign before the number.
 *
 * @FieldFormatter(
 *   id = "euro_price_formatter",
 *   label = @Translation("Euro price formatter"),
 *   field_types = {
 *     "commerce_price"
 *   }
 * )
 */
class EuroPriceFormatter extends PriceCalculatedFormatter implements ContainerFactoryPluginInterface {

  /**
   * The chain price resolver.
   *
   * @var \Drupal\commerce_price\Resolver\ChainPriceResolverInterface
   */
  protected $chainPriceResolver;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The current store.
   *
   * @var \Drupal\commerce_store\CurrentStoreInterface
   */
  protected $currentStore;

  /**
   * Constructs a new PriceCalculatedFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface $currency_formatter
   *   The currency formatter.
   * @param \Drupal\commerce_price\Resolver\ChainPriceResolverInterface $chain_price_resolver
   *   The chain price resolver.
   * @param \Drupal\commerce_store\CurrentStoreInterface $current_store
   *   The current store.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, CurrencyFormatterInterface $currency_formatter, ChainPriceResolverInterface $chain_price_resolver, CurrentStoreInterface $current_store, AccountInterface $current_user) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $currency_formatter, $chain_price_resolver, $current_store, $current_user);
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    if (!$items->isEmpty()) {
      $context = new Context($this->currentUser, $this->currentStore->getStore(), NULL, [
        'field_name' => $items->getName(),
      ]);
      /** @var \Drupal\commerce\PurchasableEntityInterface $purchasable_entity */
      $purchasable_entity = $items->getEntity();
      $resolved_price = $this->chainPriceResolver->resolve($purchasable_entity, 1, $context);
      $number = $resolved_price->getNumber();
      $currency_code = $resolved_price->getCurrencyCode();
      $options = $this->getFormattingOptions();

      $elements[0] = [
        '#theme' => 'euro_price_formatter',
        '#markup' => $this->currencyFormatter->format($number, $currency_code, $options),
        '#currency' => $currency_code,
        '#originalnumber' => $number,
        '#number' => $number,
        '#cache' => [
          'tags' => $purchasable_entity->getCacheTags(),
          'contexts' => Cache::mergeContexts($purchasable_entity->getCacheContexts(), [
            'languages:' . LanguageInterface::TYPE_INTERFACE,
            'country',
          ]),
        ],
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $entity_type = \Drupal::entityTypeManager()->getDefinition($field_definition->getTargetEntityTypeId());
    return $entity_type->entityClassImplements(PurchasableEntityInterface::class);
  }

    /**
     * {@inheritdoc}
     */
    /*public function viewElements(FieldItemListInterface $items, $langcode) {
        $elements = [];
        if (!$items->isEmpty()) {
            $context = new Context($this->currentUser, $this->currentStore->getStore());
            $purchasable_entity = $items->getEntity();
            $adjustment_types = array_filter($this->getSetting('adjustment_types'));
            $result = $this->priceCalculator->calculate($purchasable_entity, 1, $context, $adjustment_types);
            $calculated_price = $result->getCalculatedPrice();
            $originalnumber = $result->getBasePrice()->getNumber();
            $number = $calculated_price->getNumber();
            $currency = $calculated_price->getCurrencyCode();
            $options = $this->getFormattingOptions();

            $elements[0] = [
                '#theme' => 'euro_price_formatter',
                '#currency' => $currency,
                '#originalnumber' => $originalnumber,
                '#number' => $number,
                '#markup' => $this->currencyFormatter->format($number, $currency, $options),
                '#cache' => [
                    'tags' => $purchasable_entity->getCacheTags(),
                    'contexts' => Cache::mergeContexts($purchasable_entity->getCacheContexts(), [
                        'languages:' . LanguageInterface::TYPE_INTERFACE,
                        'country',
                    ]),
                ],
            ];
        }

        return $elements;
    }*/


}