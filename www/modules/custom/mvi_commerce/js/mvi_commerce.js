(function ($, Drupal) {

    Drupal.behaviors.filterModals = {
        attach: function (context, settings) {

            if ($(window).width() < 720) {
                var paginationListGenre = new List('pagination-list-wrapper-genre', {
                    valueNames: ['facet-item__value'],
                    page: 10,
                    pagination: true
                });
                var paginationListGenre = new List('pagination-list-wrapper-series', {
                    valueNames: ['facet-item__value'],
                    page:10,
                    pagination: true
                });
            }
            else {
                var paginationListGenre = new List('pagination-list-wrapper-genre', {
                    valueNames: ['facet-item__value'],
                    page: 24,
                    pagination: true
                });
                var paginationListGenre = new List('pagination-list-wrapper-series', {
                    valueNames: ['facet-item__value'],
                    page:24,
                    pagination: true
                });
            }

        }
    };

})(jQuery, Drupal);