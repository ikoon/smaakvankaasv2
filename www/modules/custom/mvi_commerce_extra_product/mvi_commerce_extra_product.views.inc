<?php

/**
 * Implements hook_views_data_alter().
 */
function mvi_commerce_extra_product_views_data_alter(array &$data) {
  $data['commerce_order_item']['mvi_commerce_extra_product_order_item_change_extra_product'] = [
    'title' => t('Extra product'),
    'field' => [
      'title' => t('Change extra product boolean'),
      'help' => t('Checkbox to change the value of the extra product.'),
      'id' => 'mvi_commerce_extra_product_order_item_change_extra_product',
    ],
  ];
}