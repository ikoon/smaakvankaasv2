This module provides a checkbox on the add to cart form "Extra brood". The price of this extra
product comes from the field 'field_brood' on the product variation.

In the cart page, the order item total uses views euro proce total formatter from the module mvi_commerce. Some additional
code is provided there to calculate the extra product in the order item total.