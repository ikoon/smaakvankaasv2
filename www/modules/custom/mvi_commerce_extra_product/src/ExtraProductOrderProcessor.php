<?php

namespace Drupal\mvi_commerce_extra_product;

use Drupal\commerce_order\Adjustment;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\OrderProcessorInterface;
use Drupal\commerce_price\Price;

/**
 * Changes order item total price when extra product is selected.
 */
class ExtraProductOrderProcessor implements OrderProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function process(OrderInterface $order) {

    foreach($order->getItems() as $orderItem){
      if($orderItem->hasField('field_brood')){
        $extraProduct = $orderItem->field_brood->value;

        // Is there an extra product on this order item selected?
        if($extraProduct){
          // Calculate the order item total price.
          $productVariation = $orderItem->getPurchasedEntity();
          $extraProductPrice = $productVariation->get('field_brood')->getValue();

          if($extraProductPrice){
            $extraProductPrice = new Price($extraProductPrice[0]['number'], $extraProductPrice[0]['currency_code']);
            $extraProductTotalPrice = $extraProductPrice->multiply($orderItem->getQuantity());

            $adjustments = $orderItem->getAdjustments();

            // Specify source_id on adjustment to combine it.
            $adjustments[] = new Adjustment([
              'type' => 'extra_product',
              'label' => t('Extra brood'),
              'amount' => $extraProductTotalPrice,
              'source_id' => 'extra_product'
            ]);

            // Calculate the correct tax to the order total
            array_push($adjustments, new Adjustment([
              'type' => 'tax',
              'label' => t('VAT'),
              'amount' => $extraProductTotalPrice->divide(121)->multiply(21),
              'percentage' => '0.21',
              'source_id' => 'btw|be|standard',
              'included' => true
            ]));

            $orderItem->setAdjustments($adjustments);
            $orderItem->save();
          }
        }
      }
    }
  }

}
