<?php

namespace Drupal\mvi_pickup;

use Drupal\Core\Database\Connection;

/**
 * Class MviPickupStorage
 *
 * @package Drupal\mvi_pickup
 */
class MviPickupStorage implements MviPickupStorageInterface {

    protected $database;

    /**
     * @param \Drupal\Core\Database\Connection $connection
     */
    public function __construct(Connection $database) {
        $this->database = $database;
    }

    public function add($date, $start, $end)
    {
        $this->database->insert('mvi_pickup_exceptions')
            ->fields(['date' => $date, 'start' => $start, 'end' => $end])
            ->execute();
    }

    public function select()
    {
        $query = $this->database->select('mvi_pickup_exceptions', 'exceptions');
        $query->orderBy('date', 'asc');
        $query->fields('exceptions');
        return $query->execute()->fetchAll();
    }

    public function delete($id)
    {
        $this->database->delete('mvi_pickup_exceptions')
            ->condition('id', $id)
            ->execute();
    }
}