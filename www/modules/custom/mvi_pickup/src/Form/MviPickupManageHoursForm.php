<?php

namespace Drupal\mvi_pickup\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mvi_pickup\MviPickupHoursStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MviPickupManageHoursForm extends FormBase {

    protected $hoursStorage;

    public function __construct(MviPickupHoursStorage $hoursStorage) {
        $this->hoursStorage = $hoursStorage;
    }

    public static function create(ContainerInterface $container) {
        return new static(
            $container->get('mvi_pickup.hours_storage')
        );
    }


    public function getFormId()
    {
        return 'mvi_pickup_manage_hours';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {

        $days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

        foreach ($days as $key => $day) {
            $form[$day] = [
                '#type' => 'fieldset',
                '#title' => $this->t($day)
            ];
            $form[$day][$day . '_start'] = [
                '#type' => 'datetime',
                '#title' => $this->t('Van:'),
                '#default_value' => new DrupalDateTime($this->hoursStorage->select($day)->start),
                '#size' => 20,
                '#required' => TRUE,
                '#date_date_element' => 'none',
                '#date_time_element' => 'time',
            ];
            $form[$day][$day . '_end'] = [
                '#type' => 'datetime',
                '#title' => $this->t('Tot:'),
                '#default_value' => new DrupalDateTime($this->hoursStorage->select($day)->end),
                '#size' => 20,
                '#required' => TRUE,
                '#date_date_element' => 'none',
                '#date_time_element' => 'time',
            ];
        }

        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Update'),
        );

        return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

        $userInput = $form_state->getValues();

        foreach ($days as $key => $day){
            /* @var DrupalDateTime $start */
            $start = $userInput[$day . '_start'];
            /* @var DrupalDateTime $end */
            $end = $userInput[$day . '_end'];

            $startTime = $start->format('H:i:s');
            $endTime = $end->format('H:i:s');

            $this->hoursStorage->update($day, $startTime, $endTime);
        }


    }

}