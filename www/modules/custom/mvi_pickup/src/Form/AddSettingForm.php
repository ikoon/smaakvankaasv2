<?php

namespace Drupal\mvi_pickup\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mvi_pickup\MviPickupStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AddSettingForm extends FormBase {

    protected $storage;

    public function __construct(MviPickupStorage $storage) {
        $this->storage = $storage;
    }

    public static function create(ContainerInterface $container) {
        return new static(
            $container->get('mvi_pickup.storage')
        );
    }


    public function getFormId()
    {
        return 'test_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {

        $form['date'] = [
            '#type' => 'datetime',
            '#title' => $this->t('Datum:'),
            '#size' => 20,
            '#default_value' => new DrupalDateTime(),
            '#required' => TRUE,
            '#date_date_element' => 'date',
            '#date_time_element' => 'none',
        ];
        $form['start'] = [
            '#type' => 'datetime',
            '#title' => $this->t('Van:'),
            '#size' => 20,
          '#default_value' => new DrupalDateTime(),
            '#required' => TRUE,
            '#date_date_element' => 'none',
            '#date_time_element' => 'time',
        ];
        $form['end'] = [
            '#type' => 'datetime',
            '#title' => $this->t('Tot:'),
            '#size' => 20,
          '#default_value' => new DrupalDateTime(),
            '#required' => TRUE,
            '#date_date_element' => 'none',
            '#date_time_element' => 'time',
        ];

        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Add'),
        );

        return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $userInput = $form_state->getValues();

        /* @var DrupalDateTime $day */
        $day = $userInput['date'];
        $date = strtotime($day);

        /* @var DrupalDateTime $start */
        $start = $userInput['start'];
        /* @var DrupalDateTime $end */
        $end = $userInput['end'];

        // $date = $day->format('j/m/Y');
        $startTime = $start->format('H:i:s');
        $endTime = $end->format('H:i:s');

        $this->storage->add($date, $startTime, $endTime);

        drupal_set_message('De instelling werd opgeslagen.');
        $form_state->setRedirect('mvi_pickup.settings');
    }

}