<?php

namespace Drupal\mvi_pickup\Form;

use Drupal\Console\Bootstrap\Drupal;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mvi_pickup\MviPickupStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure watch later settings form
 */
class PickupManageSettingsForm extends FormBase {

    protected $storage;

    public function __construct(MviPickupStorage $storage) {
        $this->storage = $storage;
    }

    public static function create(ContainerInterface $container) {
        return new static(
            $container->get('mvi_pickup.storage')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'pickup_manage_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {

       $results = $this->storage->select();

       foreach($results as $result){

           $options[$result->id] = [
               'date' => DrupalDateTime::createFromTimestamp($result->date)->format('d/m/Y'),
               'start' => $result->start,
               'end' => $result->end,
           ];
       }

        // Prepare the header for the tableselect.
        $header = [
            'date' => t('Datum'),
            'start' => t('Van'),
            'end' => t('Tot'),
        ];


        // Add a tableselect to the form with the subscribed nodes.
        $form['settings_select'] = array(
            '#type' => 'tableselect',
            '#header' => $header,
            '#options' => isset($options) ? $options : [],
            '#empty' => t('No content available'),
        );

        // Add a remove button.
        $form['actions'] = ['#type' => 'actions'];
        if (!empty($options)) {
            $form['actions']['submit'] = [
                '#type' => 'submit',
                '#value' => 'Verwijder de instellingen',
            ];
        }

        return $form;

    }

    public function submitForm(array &$form, FormStateInterface $form_state) {
        // Get the values from the node_select form item.
        $selected_settings = $form_state->getValue('settings_select');

        // Remove the selected nodes from the watch later list.
        if (!empty($selected_settings)) {
            foreach ($selected_settings as $selected_setting) {
                if($selected_setting != 0)
                $this->storage->delete($selected_setting);
            }
        }
        drupal_set_message('De instelling(en) werd(en) verwijderd');

    }

}