<?php

namespace Drupal\mvi_pickup\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the shipping information pane.
 *
 * Collects the shipping profile, then the information for each shipment.
 * Assumes that all shipments share the same shipping profile.
 *
 * @CommerceCheckoutPane(
 *   id = "pickup_order",
 *   label = @Translation("Pick up order"),
 *   wrapper_element = "fieldset",
 * )
 */
class PickupOrder extends CheckoutPaneBase implements ContainerFactoryPluginInterface
{

    /**
     * Constructs a new CheckoutPaneBase object.
     *
     * @param array $configuration
     *   A configuration array containing information about the plugin instance.
     * @param string $plugin_id
     *   The plugin_id for the plugin instance.
     * @param mixed $plugin_definition
     *   The plugin implementation definition.
     * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface $checkout_flow
     *   The parent checkout flow.
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
     *   The entity type manager.
     */
    public function __construct(array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow, EntityTypeManagerInterface $entity_type_manager)
    {
        parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager);

        $this->checkoutFlow = $checkout_flow;
        $this->order = $checkout_flow->getOrder();
        $this->setConfiguration($configuration);
        $this->entityTypeManager = $entity_type_manager;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL)
    {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $checkout_flow,
            $container->get('entity_type.manager')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function buildPaneSummary() {

        $date = $this->order->get('field_pickup_from')->getValue();
        $from = $this->order->get('field_pickup_from')->getValue();
        $to = $this->order->get('field_pickup_to')->getValue();

        $summary = "Ophalen op: <strong>" . date('d/m/Y', $date[0]['value']) . "</strong> tussen <strong>" . date('H:i', $from[0]['value']) . "</strong> en <strong>" . date('H:i',$to[0]['value']) . '</strong>';

        return $summary;
    }

    /**
     * {@inheritdoc}
     */
    public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form)
    {
        $now = new \DateTime();
        $date = $this->order->get('field_pickup_from')->getValue()[0]['value'];
        $from = $this->order->get('field_pickup_from')->getValue()[0]['value'];
        $to = $this->order->get('field_pickup_to')->getValue()[0]['value'];

        $pane_form['date'] = [
            '#title' => $this->t('Datum'),
            '#type' => 'date',
            '#attributes' => [
                'type' => 'date',
                'min' => 'now',
                'max' => '+1 years'
            ],
            '#date_date_format' => 'dd/mm/Y',
        ];

        $pane_form['between'] = array(
            '#type' => 'fieldset',
            '#title' => $this->t('Uur'),
        );

        $pane_form['between']['start'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Start'),
            '#size' => 60,
            '#maxlength' => 128,
            '#required' => TRUE,
            '#prefix' => 'Tussen ',
            '#suffix' => ' en '
        );

        $pane_form['between']['end'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('End'),
            '#size' => 60,
            '#maxlength' => 128,
            '#required' => TRUE
        );

        if($now->getTimestamp() < $date){
            $pane_form['date']['#attributes']['data-value'] = date('Y/m/d', $date);
            $pane_form['between']['start']['#attributes']['data-value'] = date('H:i', $from);
            $pane_form['between']['end']['#attributes']['data-value'] = date('H:i', $to);
        }

        return $pane_form;
    }

    public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {

        $pickupData = $form_state->getValue('pickup_order');

        $date = \DateTime::createFromFormat('Y/m/d', $pickupData['date']);
        $from = \DateTime::createFromFormat("H:i", $pickupData['between']['start']);
        $to = \DateTime::createFromFormat("H:i", $pickupData['between']['end']);

        $startDate = $date->setTime($from->format('H'), $from->format('i'), 0);
        $this->order->set('field_pickup_from', $startDate->getTimestamp());

        $endDate = $date->setTime($to->format('H'), $to->format('i'), 0);
        $this->order->set('field_pickup_to', $endDate->getTimestamp());

        $this->order->save();
    }

}
