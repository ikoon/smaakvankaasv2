<?php

use Drupal\mvi_pickup\MviPickupHoursStorage;
use Drupal\mvi_pickup\MviPickupStorage;

/**
 * Implements hook_theme().
 */
function mvi_pickup_theme($existing, $type, $theme, $path) {
    $variables = array(
        'mvi_pickup_manage' => array(
            'variables' => array(
                'exceptions' => [],
                'form' => [],
            ),
            'template' => 'manage',
        ),
    );
    return $variables;
}

function mvi_pickup_form_commerce_checkout_flow_multistep_default_alter(&$form, &$form_state, $form_id) {
    // Attach the mvi_pickup library to the checkout form.

    /* @var MviPickupHoursStorage $hourStorage */
    $hourStorage = \Drupal::service('mvi_pickup.hours_storage');
    /* @var MviPickupStorage $exceptionStorage */
    $exceptionStorage = \Drupal::service('mvi_pickup.storage');

    $days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
    $openingHours = [];
    foreach ($days as $day){
        $openingHours[$day] = $hourStorage->select($day);
    }

    $exceptions = $exceptionStorage->select();
    $form['#cache'] = ['max-age' => 0];
    $form['#attached']['library'][] = 'mvi_pickup/mvi_pickup';
    $form['#attached']['drupalSettings']['mvi_pickup']['openinghours'] = $openingHours;
    $form['#attached']['drupalSettings']['mvi_pickup']['exceptions'] = $exceptions;

}

/**
 * Implements hook_form_alter().
 */
function mvi_pickup_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  if($form['#id'] == 'views-exposed-form-commerce-orders-page-1'){

    // Change the input type so a html 5 date popup appears.
    $form['field_pickup_from_value']['min']['#type'] = 'date';
    $form['field_pickup_from_value']['max']['#type'] = 'date';
    $form['#attached']['library'][] = 'mvi_pickup/mvi_pickup';
  }
}

/**
 * Implements hook_views_pre_view().
 *
 * Convert the date input string to timestamp so correct filtering can be done.
 */
function mvi_pickup_views_pre_view(\Drupal\views\ViewExecutable $view, $display_id, array &$args) {
  switch ($view->id()) {
    case 'commerce_orders':
      // Get exposed filters value
      $filter_input = $view->getExposedInput();
      if(array_key_exists('field_pickup_from_value', $filter_input)){
        $min = $filter_input['field_pickup_from_value']['min'];
        $max = $filter_input['field_pickup_from_value']['max'];

        if($min && $max){
          // Set exposed filter value to new one
          $minDate = new DateTime($min);
          $maxDate = new DateTime($max);
          $filter_input['field_pickup_from_value'] = ['min' => $minDate->getTimestamp(), 'max' => $maxDate->getTimestamp()];
          $view->setExposedInput($filter_input);
        }
      }

      break;
  }
}
