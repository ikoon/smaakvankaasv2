(function ($, window) {
  "use strict";
  /**
   * Pickup js
   */
  Drupal.behaviors.pickupOrder = {
    attach: function (context,settings) {

      var dateInput = $('#edit-pickup-order-date');
      var endInput = $('#edit-pickup-order-between-end').pickatime({format: 'HH:i'});
      var endPicker = endInput.pickatime('picker');
      endPicker.set('disable', true);
      endPicker.set('enable', false);

      var startInput = $('#edit-pickup-order-between-start').pickatime({
        format: 'HH:i',
        onSet: function(context){
          if(typeof context.select !== 'undefined'){
            _setAvailableEndTimes($('#edit-pickup-order-date').val(), $('#edit-pickup-order-between-start').val(),endPicker,settings);
          }
        }
      });
      var startPicker = startInput.pickatime('picker');
      startPicker.set('disable', true);
      startPicker.set('enable', false);

      var closedDays = _returnClosedDays(settings.mvi_pickup.openinghours, settings.mvi_pickup.exceptions);
      closedDays.push(new Date());

      dateInput.pickadate({
        min: new Date('d/m/yyyy'),
        format: 'd/m/yyyy',
        disable: closedDays,
        onSet: function(context){
          if(context.select){
            // Clear the time inputs
            endInput.val('');
            startInput.val('');
            _setAvailableStartTimes(context.select,startPicker,settings);
          }
        }
      });

      // If there is a default date provided.
      if(dateInput.val()){
        var dsplit = dateInput.val().split("/");
        var d = new Date(dsplit[2],dsplit[1]-1,dsplit[0]);

        _setAvailableStartTimes(d.getTime(),startPicker,settings);
        _setAvailableEndTimes($('#edit-pickup-order-date').val(), $('#edit-pickup-order-between-start').val(),endPicker,settings);
      }

    }
  };

  // Transform the timestamp value in the input fields on order admin page.
  Drupal.behaviors.transformTimestamp = {
    attach: function (context, settings) {
      if($('#views-exposed-form-commerce-orders-page-1').length){
        var exposedForm = $('#views-exposed-form-commerce-orders-page-1');
        var minValue = exposedForm.find('#edit-field-pickup-from-value-min').attr('value');
        var maxValue = exposedForm.find('#edit-field-pickup-from-value-max').attr('value');
        if(minValue && maxValue) {
          var minDate = new Date(minValue * 1000);
          var minDay = minDate.getDate();
          var minMonth = minDate.getUTCMonth() + 1;
          var minYear = minDate.getFullYear();
          console.log(minDay + ' ' + minMonth);
          exposedForm.find('#edit-field-pickup-from-value-min').val(minYear + '-' + minMonth + '-' + minDay);

          var maxDate = new Date(maxValue * 1000);
          var maxDay = maxDate.getDate();
          var maxMonth = maxDate.getUTCMonth() + 1;
          var maxYear = maxDate.getFullYear();
          exposedForm.find('#edit-field-pickup-from-value-max').val(maxYear + '-' + maxMonth + '-' + maxDay);
        }
      }
    }
  };

  function _setAvailableStartTimes(timestamp, startPicker, settings){
    var d = new Date(timestamp);
    var dayOfWeek = d.getDay();
    var openingHours = _returnOpeningHours(dayOfWeek, d, settings);

    // openingHours.start is formatted like 'hh:mm:ss' so you can use substring
    var fromHour = parseInt(openingHours.start.substring(0,2));
    var fromMinute = parseInt(openingHours.start.substring(3,6));
    var endHour = parseInt(openingHours.end.substring(0,2));
    var endMinute = parseInt(openingHours.end.substring(3,6));

    // Reset the timepickers.
    startPicker.set('disable', true);
    startPicker.set('enable', false);

    startPicker.set('disable',[
      {from: [fromHour,fromMinute], to: [endHour,endMinute - 30]}
    ]);

  }

  function _setAvailableEndTimes(datestring, startTime, endPicker, settings){
    var dsplit = datestring.split("/");
    var d = new Date(dsplit[2],dsplit[1]-1,dsplit[0]);
    var dayOfWeek = d.getDay();

    var openingHours = _returnOpeningHours(dayOfWeek, d, settings);

    // openingHours.start is formatted like 'hh:mm:ss' so you can use substring
    var fromHour = parseInt(startTime.substring(0,2));
    var fromMinute = parseInt(startTime.substring(3,6));
    var endHour = parseInt(openingHours.end.substring(0,2));
    var endMinute = parseInt(openingHours.end.substring(3,6));

    // Reset the timepickers.
    endPicker.set('disable', true);
    endPicker.set('enable', false);
    if(fromHour == endHour && fromMinute == endMinute){

    } else {
      // We flipped enable and disable, so use disable property to enable given hours.
      endPicker.set('disable',[
        {from: [fromHour,fromMinute + 30], to: [endHour,endMinute]}
      ]);
    }

  }

  function _returnOpeningHours(dayOfWeek, date, settings){

    var openingHoursData = settings.mvi_pickup.openinghours;
    var exceptions = settings.mvi_pickup.exceptions;

    var exceptionObj = false;

    $.each(exceptions, function (key, exception) {
      if(date.setHours(0,0,0,0) == new Date(exception.date * 1000).setHours(0,0,0,0)){
        exceptionObj = exception;
      }
    });

    if( !exceptionObj ){
      if(dayOfWeek == 1){
        return openingHoursData.monday;
      } else if (dayOfWeek == 2){
        return openingHoursData.tuesday;
      } else if (dayOfWeek == 3){
        return openingHoursData.wednesday;
      } else if (dayOfWeek == 4){
        return openingHoursData.thursday;
      } else if (dayOfWeek == 5){
        return openingHoursData.friday;
      } else if (dayOfWeek == 6){
        return openingHoursData.saturday;
      } else if (dayOfWeek == 0){
        return openingHoursData.sunday;
      }
    } else {
      return exceptionObj;
    }
  }

  function _returnClosedDays(daysOfWeek, exceptions){

    var days = [];

    if(daysOfWeek.monday.start == daysOfWeek.monday.end){
      days.push(1);
    }
    if(daysOfWeek.tuesday.start == daysOfWeek.tuesday.end){
      days.push(2);
    }
    if(daysOfWeek.wednesday.start == daysOfWeek.wednesday.end){
      days.push(3);
    }
    if(daysOfWeek.thursday.start == daysOfWeek.thursday.end){
      days.push(4);
    }
    if(daysOfWeek.friday.start == daysOfWeek.friday.end){
      days.push(5);
    }
    if(daysOfWeek.saturday.start == daysOfWeek.saturday.end){
      days.push(6);
    }
    if(daysOfWeek.sunday.start == daysOfWeek.sunday.end){
      days.push(0);
    }

    // Add tomorrow in the closed days
    var today = new Date();
    var tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);
    days.push(tomorrow);

    // Exceptions
    $.each(exceptions, function (key, exception) {
      if(exception.start == exception.end){
        var date = new Date(exception.date * 1000);
        days.push(date);
      } else {
        // If the exception is an available day, remove the day from the array.
        var openDate = new Date();
        openDate.setTime(exception.date * 1000); // javascript timestamps are in milliseconds
        days.push([openDate.getUTCFullYear(), openDate.getMonth(), openDate.getUTCDate(), 'inverted']);
      }
    });

    return days;
  }


})(jQuery, window);
